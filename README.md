Sheep for Gretro
----------------------------------------------------------------------------
Sheep for [Gretro] is an implementation [sheep] for HTML.

Put your web site
----------------------------------------------------------------------------
``` html
<canvas id="woolmark_canvas" width="100" height="100"></canvas>
<script type="text/javascript" src="gretro.min.js"></script>
<script type="text/javascript" src="gretro-text.js"></script>
<script type="text/javascript" src="gretro-image.js"></script>
<script type="text/javascript" src="sheep.js" ></script>
```

License
----------------------------------------------------------------------------
[wtfpl] 

[sheep]: https://bitbucket.org/runne/woolmark "Sheep"
[Gretro]: https://github.com/mohayonao/gretro
[wtfpl]: http://www.wtfpl.net "WTFPL"
