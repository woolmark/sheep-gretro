(function(grImage) {
  "use strict";

  // Module systems magic dance.

  if (typeof require === "function" && typeof exports === "object" && typeof module === "object") {
    // NodeJS
    module.exports = grImage;
  } else if (typeof define === "function" && define.amd) {
    // AMD
    define(function () {
      return grImage;
    });
  } else {
    // Other environment (usually <script> tag): plug in to global chai instance directly.
    gretro.use(grImage);
  }

})(function(gr, _) {
  "use strict";

  /**
   * drawImage
   * draw a image to the canvas
   *
   * @param {Image|Canvas} image
   * @param {int} x
   * @param {int} y
   */
  gr.Canvas.addMethod("drawImage", function(image, x, y) {

    if (image != undefined) {
      _.stroke(this, function(color) {
        
        var canvas = document.createElement("canvas");
        var context = canvas.getContext('2d');
        canvas.width = image.width;
        canvas.height = image.height;
        context.drawImage(image, 0, 0);

        for (var w = 0; w < image.width; w++) {
          for (var h = 0; h < image.height; h++) {

            var data = context.getImageData(w, h, 1, 1).data;
            var r = data[0];
            var g = data[1];
            var b = data[2];
            var a = data[3];

            if (r == 0 && g == 0 && b == 0 && a == 0) {
              continue;
            }
            
            var color = 0;
            var distance = Number.MAX_VALUE;
            for (var c = 0; c < 16; c++) {
              var rgb = this.getColor(c);
              var cr = rgb >> 16 & 0xFF;
              var cg = rgb >> 8 & 0xFF;
              var cb = rgb & 0xFF;
              
              var d = Math.pow(r - cr, 2) + Math.pow(g - cg, 2) + Math.pow(b - cb, 2);
              if (d < distance) {
                distance = d;
                color = c;
              }
            }
            
            _.putPixel(this, x + w, y + h, color);

          }
        }

      });
    }

  });

});
